`import Ember from 'ember'`
`import layout from './template'`

MatportDrawerComponent = Ember.Component.extend
  layout: layout
  classNames: ["matport-drawer", "mdl-layout__drawer"]
  classNameBindings: ["showDrawer:is-visible:"]
  navLinkClass: "mdl-navigation__link"
  footLinkClass: "mdl-navigation__link"

`export default MatportDrawerComponent`
