`import Ember from 'ember'`
`import layout from './template'`

{computed: {gte, not: oppositeOf}, inject: {service}} = Ember

MatportHeaderComponent = Ember.Component.extend
  tagName: "header"
  layout: layout
  classNames: ["matport-header", "mdl-layout__header", "mdl-layout__header--waterfall"]
  navLinkClass: "mdl-navigation__link matport-header__navlink"
  classNameBindings: ["isCompact:is-compact:", "isCompact:is-casting-shadow:"]
  isCompact: false
  device: service "device/layout"

  isAtMostDesktop: oppositeOf "device.isAtLeastDesktop"

`export default MatportHeaderComponent`
