`import Ember from 'ember'`
`import layout from './template'`

{computed: {gte}, inject: {service}} = Ember

MatportLayoutComponent = Ember.Component.extend
  layout: layout
  classNames: ["matport-layout__container", "mdl-layout__container"]
  navLinks: []
  footLinks: []
  showDrawer: false
  isHeaderCompact: gte "scrollTop", 2.5
  matportEnv: service "matport-environment"

  didInsertElement: ->
    @$(".matport-layout__content").on "scroll", (e) =>
      @onScroll(e, @$(".matport-layout__content").scrollTop())
  willDestroyElement: ->
    @$(".matport-layout__content").off "scroll"

  onScroll: (e, top) ->
    @set('scrollTop', top)

`export default MatportLayoutComponent`
