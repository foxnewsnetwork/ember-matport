`import Ember from 'ember'`
`import layout from './template'`

{String, computed, isPresent} = Ember

MatportBigCardComponent = Ember.Component.extend
  layout: layout
  classNames: ["matport-big-card", "mdl-cell", "mdl-cell--12-col", "mdl-card", "mdl-shadow--4dp"]
  title: "About"
  titleStyle: computed "bgSrc", ->
    src = @get "bgSrc"
    return unless isPresent src
    String.htmlSafe """
    background-image: url(#{src});
    """

`export default MatportBigCardComponent`
