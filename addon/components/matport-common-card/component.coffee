`import Ember from 'ember'`
`import layout from './template'`
`import _ from 'lodash/lodash'`

{A, isPresent, String, computed} = Ember
{map} = computed

zip = (key1, key2) ->
  computed key1, key2, ->
    _.zip @get(key1), @get(key2)

MatportCommonCardComponent = Ember.Component.extend
  layout: layout
  classNames: ["matport-common-card__container", "mdl-shadow--2dp", "mdl-cell"]
  classNameBindings: ["colClass"]
  sizeNumbers: [3, 4, 6, 12]
  sizeDevices: ["", "desktop", "tablet", "phone"]
  sizeRawTails: zip "sizeNumbers", "sizeDevices"
  sizeClasses: map "sizeRawTails", ([number, device]) ->
    A ["mdl-cell-", number, "col", device]
    .filter isPresent
    .join "-"
  colClass: computed "sizeClasses", ->
    @get "sizeClasses"
    .join " "
  titleStyle: computed "bgSrc", ->
    src = @get "bgSrc"
    return unless isPresent src
    String.htmlSafe """
    background-image: url(#{src});
    """

`export default MatportCommonCardComponent`
