`import Ember from 'ember'`
`import layout from './template'`

MatportFooterComponent = Ember.Component.extend
  tagName: "footer"
  layout: layout
  classNames: ["matport-footer","mdl-mini-footer"]
  classNameBindings: ["showFooter::hidden"]
  footLinkClass: "matport-footer__link mdl-typography--font-light"
  title: "Pisources Company"

`export default MatportFooterComponent`
