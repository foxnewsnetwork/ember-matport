/* jshint node: true */

module.exports = function(/*environment*/) {
  return { 
    matport: {
      components: {
        header: "matport-header",
        footer: "matport-footer",
        drawer: "matport-drawer"
      }
    }
  };
};
