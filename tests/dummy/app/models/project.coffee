`import DS from 'ember-data'`
`import Fixtures from '../fixtures/projects'`

Project = DS.Model.extend
  name: DS.attr "string",
    label: "Project Name"
    description: "The project name"
    modify: ["new", "edit"]
    display: ["show", "index"]

  summary: DS.attr "string",
    label: "Project Summary"
    description: "A short one-sentence description of what this project is about"
    modify: ["new", "edit"]
    display: ["show"]

  description: DS.attr "string",
    label: "Full Description"
    description: "The markdown-flavored description of project"
    modify: ["new", "edit"]
    display: ["show"]

  codeLink: DS.attr "string",
    label: "Repo Link"
    description: "The place where this code is located"
    modify: ["new", "edit"]
    display: ["show"]

  exampleLink: DS.attr "string",
    label: "Live Site Link"
    description: "The live version of this site"
    modify: ["new", "edit"]
    display: ["show"]

  leadImage: DS.attr "string",
    label: "Lead Image"
    description: "The lead showcase image of this project"
    modify: ["new", "edit"]
    display: ["show"]

Project.reopenClass
  FIXTURES: Fixtures

`export default Project`
