`import Ember from 'ember'`

# This function receives the params `params, hash`
querystring = (params, hash) ->
  Ember.$.param hash

QuerystringHelper = Ember.Helper.helper querystring

`export { querystring }`

`export default QuerystringHelper`
