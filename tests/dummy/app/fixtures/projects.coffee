Violence =
  id: "violence"
  name: "Violence Dynamics"
  summary: "Landing site and store-front for selling seminar tickets"
  description: """
  ## The Organization
  Violence Dynamics is a group of force professionals who go about the country and teach conflict resolution, self-defense, and staying safe.

  ## The Website
  Built with Google's Material Design concepts, this site is a fully responsive mobile-optized, and properly SEO'd single page app Ember.js application.

  The back-end is powered by a combination of Ruby on Rails Spree and Wordpress. The front-end is built with Ember.js and Selenium
  """
  codeLink: "https://gitlab.com/foxnewsnetwork/storefront"
  exampleLink: "https://foxnewsnetwork.gitlab.io/storefront/"
  leadImage: "assets/images/violence.jpg"
Warehouse =
  id: "warehouse"
  name: "SimWMS - Warehouse Management"
  summary: "Cloud-based team warehouse management service"
  description: """
  ## The Organization
  SimWMS is a cloud-based warehouse management service for handling appointments, trucks, weight tickets, inventory, and employees.

  ## The App
  A web and desktop app, SimWMS is like a specialized Dropbox for managing warehouse assets. 

  On the backend, warehouse is powered on Heroku by Elixir Phoenix, postgresql, and elasticsearch. 

  The front-end is built with AutoX Ember.js deployed via Pagefront for web and Github Electron for desktop.
  """
  exampleLink: "https://warehouse.pagefrontapp.com/"
  leadImage: "assets/images/warehouse-hero.jpg"
Recycle =
  id: "recycle"
  name: "ML Recycling"
  summary: "Company store-front site for plastics trade"
  description: """
  ## The Organization
  ML Recycling is a Los Angeles-based plastic scraps recycling and trade company.

  ## The Website
  The site is built with Ember.js on materialize. Despite being a full-functioning storefront, the back-end is hosted on tumblr using a custom json protocol built on top of tumblr's api.
  """
  codeLink: "https://github.com/mlrecycling/mlrecycling.github.io"
  exampleLink: "https://mlrecycling.github.io"
  leadImage: "http://mlrecycling.github.io/images/recycle.png"
Harbor =
  id: "harbor"
  name: "Harbor Green Group"
  summary: "Company website"
  codeLink: "https://github.com/harborgreen/harborgreen.github.io"
  exampleLink: "http://www.harborgreengroup.com/"
  leadImage: "assets/images/harbor.png"
Amagami =
  id: "amagami"
  name: "Amagami Challenge"
  summary: "Anime-inspired dating site"
  description: """
  ## The Organization
  Hailing straight out of the internet, the Amagami Challenge is merely a self-imposed challenge for the modern self-enclosed computer-bond person to set outside his or her internet bubble and rediscover life.

  ## The App
  A web and mobile app, Amagami Challenge is built with Ember.js and material design on the front end, and ruby rails on the backend.
  """
  codeLink: "https://github.com/foxnewsnetwork/amagami"
  exampleLink: "http://amagamichallenge.org/"
  leadImage: "assets/images/amagami.png"
Lacekei =
  id: "lacekei"
  name: "Lacekei Market Place"
  summary: "One stop shop for custom dresses and cosplay"
  codeLink: "https://github.com/lacekei/lacekei.github.io"
  exampleLink: "https://lacekei.github.io"
  leadImage: "assets/images/dress.png"
Fixtures = [Violence, Warehouse, Amagami, Recycle, Harbor, Lacekei]
`export default Fixtures`