{computed: {alias}, inject: {service}} = Ember
MatportEnvironmentService = Ember.Service.extend
  config: service()
  headerComponent: alias "config.matport.components.header"
  footerComponent: alias "config.matport.components.footer"
  drawerComponent: alias "config.matport.components.drawer"
  companyName: alias "config.companyName"