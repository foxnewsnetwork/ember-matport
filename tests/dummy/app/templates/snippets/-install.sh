npm install --save-dev git+https://gitlab.com/pisource/ember-matport.git

npm install --save-dev git+https://gitlab.com/pisource/simwms.git#v0.1.4
npm install --save-dev git+https://gitlab.com/foxnewsnetwork/storefront.git#v0.1.4

ember install flexi
ember install ember-material-lite
ember install ember-font-awesome
ember install ember-lodash
ember install ember-config-service
ember install ember-composable-helpers
ember install ember-block-slots

bower install --save material-design-lite

## ember-cli-build.js
var EmberAddon = require('ember-cli/lib/broccoli/ember-addon');
var shim = require('flexi/lib/pod-templates-shim');
 
shim(EmberAddon);

module.exports = function(defaults) {
  var app = new EmberAddon(defaults, {
    sassOptions: {
      includePaths: ['bower_components/material-design-lite/src']
    }
  });

  /*
    This build file specifies the options for the dummy test app of this
    addon, located in `/tests/dummy`
    This build file does *not* influence how the addon or the app using it
    behave. You most likely want to be modifying `./index.js` or app's build file
  */

  return app.toTree();
};
