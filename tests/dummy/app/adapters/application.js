import FixtureAdapter from 'ember-data-fixture-adapter';

export default FixtureAdapter.extend({
  findRecord() {
    return this.find.apply(this, arguments);
  }
});