`import Ember from 'ember'`
`import config from './config/environment'`

Router = Ember.Router.extend
  location: config.locationType

Router.map ->
  @route "blog"
  @route "about"
  @route "terms"
  @route "privacy"
  @route "projects", path: "/showcase", ->
  @route "project", path: "/project/:project_id", ->
  @route "requests", path: "/requests", ->
    @route "new"

  @route "goods", path: "/goods", ->
    @route "requirements"
    @route "layout"
    @route "big-card"
    @route "common-card"
    @route "env"

`export default Router`
