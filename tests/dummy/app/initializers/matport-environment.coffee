# Takes two parameters: container and application
initialize = (application) ->
  application.inject "controller", "matportEnv", "service:matport-environment"

MatportEnvironmentInitializer =
  name: 'matport-environment'
  initialize: initialize

`export {initialize}`
`export default MatportEnvironmentInitializer`
