// Here's your chance to specify what routes will be statically pre-rendered
module.exports = [
  "/", 
  "about", 
  "showcase", 
  "requests/new",
  "goods",
  "goods/layout",
  "goods/big-card",
  "goods/common-card",
  "goods/env"
];